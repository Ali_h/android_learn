package com.example.ali.sqlitetest;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //openOrCreateDatabase() : Open a new private SQLiteDatabase associated with this Context's application package. Create the database file if it doesn't exist.
        //name	String: The name (unique in the application package) of the database.
        //mode	int: Operating mode.
        //Value is either 0 or combination of MODE_PRIVATE, MODE_WORLD_READABLE, MODE_WORLD_WRITEABLE, MODE_ENABLE_WRITE_AHEAD_LOGGING or MODE_NO_LOCALIZED_COLLATORS.
        //factory SQLiteDatabase.CursorFactory: An optional factory class that is called to instantiate a cursor when query is called.
        SQLiteDatabase sqLiteDatabase = getBaseContext().openOrCreateDatabase("sqlite-test.db" , MODE_PRIVATE , null);
        String sql = "DROP TABLE IF EXISTS contacts";
        sqLiteDatabase.execSQL(sql);
        sql = "CREATE TABLE IF NOT EXISTS contacts(name TEXT, phone INTEGER, email TEXT);";
        Log.d(TAG, "onCreate: sql is : " + sql);
        sqLiteDatabase.execSQL(sql);//execSQL() : Execute a single SQL statement that is NOT a SELECT or any other SQL statement that returns data.
        sql = "INSERT INTO contacts VALUES('ali' , 123456 , 'ali@email.com');";
        Log.d(TAG, "onCreate: sql is : " + sql);
        sqLiteDatabase.execSQL(sql);
        sql = "INSERT INTO contacts VALUES('navid' , 654321 , 'navid@email.com');";
        Log.d(TAG, "onCreate: sql is : " + sql);
        sqLiteDatabase.execSQL(sql);

        Cursor query = sqLiteDatabase.rawQuery("SELECT * FROM contacts;" , null);
        if(query.moveToFirst()){
            do{
                String name = query.getString(0);
                int phone = query.getInt(1);
                String email = query.getString(2);
                Toast.makeText(this, "name = " + name + " phone = " + phone + " email = " + email, Toast.LENGTH_LONG).show();
            } while (query.moveToNext());
        }
        query.close();
        sqLiteDatabase.close();
    }
}
