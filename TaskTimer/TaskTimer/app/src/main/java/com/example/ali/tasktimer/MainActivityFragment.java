package com.example.ali.tasktimer;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.security.InvalidParameterException;

/**
 * A placeholder fragment containing a simple view.
 * <p>
 * chon in class ast ke recycler view ro neshoon mide pas dar in class ba recycler va loader ha kar mikonim
 */
public class MainActivityFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = "MainActivityFragment";

    private static final int LOADER_ID = 0;

    private CursorRecyclerViewAdapter mAdapter;//to set this adapter for recycler

    public MainActivityFragment() {
        Log.d(TAG, "MainActivityFragment: constructor called");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: starts");
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.task_list);//in this method we find recyler for use this in this class
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));//init this recycler

        mAdapter = new CursorRecyclerViewAdapter(null, (CursorRecyclerViewAdapter.OnTaskClickListener) getActivity());
        recyclerView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {//called after onCreateView
        Log.d(TAG, "onActivityCreated: starts");
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {//Instantiate and return a new Loader for the given ID.(id hamoon id loader has ke besh dadim)
        Log.d(TAG, "onCreateLoader: starts with id : " + id);

        String[] projection = {TaskContract.Columns.TASKS_ID, TaskContract.Columns.TASKS_NAME,
                TaskContract.Columns.TASKS_DESCRIPTION, TaskContract.Columns.TASKS_SORTORDER};
        String sortData = TaskContract.Columns.TASKS_SORTORDER + "," + TaskContract.Columns.TASKS_NAME + " COLLATE NOCASE";

        switch (id) {
            case LOADER_ID:
                Log.d(TAG, "onCreateLoader: starts cursor loader");
                return new CursorLoader(getActivity(),//this method run the qurey mthod in provider in backgournd thread
                        TaskContract.CONTENT_URI,
                        projection,
                        null,
                        null,
                        sortData);
            //cursorLoader mese query ast ba in fargh ke dar background ejra mishe

            default:
                throw new InvalidParameterException(TAG + "oncreateloader called with invalid loader id " + id);//why InvalidParameterException?
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {//bade inke onCreateLoader data ro retrive kard be manager mige va in ejra mishe (callback)
        //dar in method be hich vajh nabayad cursor ro close kard chon in cursor vase ma nis balke tavasotte loader dare faraham mishe
        Log.d(TAG, "onLoadFinished: starts");
        mAdapter.swapCursor(data);
        int count = mAdapter.getItemCount();
        Log.d(TAG, "onLoadFinished: the data comes back from cursor is : " + count);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.d(TAG, "onLoaderReset: starts");
        mAdapter.swapCursor(null);
    }
}
