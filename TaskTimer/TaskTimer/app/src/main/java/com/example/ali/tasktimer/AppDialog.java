package com.example.ali.tasktimer;

import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;

/**
 * Created by ali on 10/29/17.
 */

public class AppDialog extends DialogFragment {
    private static final String TAG = "AppDialog";

    public static final String DIALOG_ID = "id";
    public static final String DIALOG_MESSAGE = "message";
    public static final String DIALOG_POSITIVE_RID = "positive_rid";
    public static final String DIALOG_NEGATIVE_RID = "negative_rid";

    //create interface for handle the event in dialog
    interface DialogEvent {
        void onPositiveDialogResualt(int dialogId, Bundle args);

        void onNegativeDialogResualt(int dialogId, Bundle args);

        void onDialogCancelled(int dialogId);
    }

    private DialogEvent mDialogEvent;

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach: starts , activity is : " + context.toString());
        super.onAttach(context);

        //activities must implement this interface
        if (!(context instanceof DialogEvent)) {
            throw new ClassCastException(context.toString() + " must implement DialogFragment.DialogEvent interface");
        }

        mDialogEvent = (DialogEvent) context;
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach: strts");
        super.onDetach();

        mDialogEvent = null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.d(TAG, "onCreateDialog: starts");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final Bundle arguments = getArguments();
        final int dialogId;
        int positiveStringId;
        int negativeStringId;
        String messageString;

        if (arguments != null) {
            dialogId = arguments.getInt(DIALOG_ID);
            messageString = arguments.getString(DIALOG_MESSAGE);
            //chon dialog_id va dialog_message mohem hastan bayad check shavand ke khali nabashan
            if (dialogId == 0 || messageString == null) {
                throw new IllegalArgumentException("dialog id and/or dialog message in not present");
            }

            positiveStringId = arguments.getInt(DIALOG_POSITIVE_RID);
            if (positiveStringId == 0) {
                positiveStringId = R.string.ok;
            }
            negativeStringId = arguments.getInt(DIALOG_NEGATIVE_RID);
            if (negativeStringId == 0) {
                negativeStringId = R.string.cancel;
            }
        } else {
            throw new IllegalArgumentException("must init bundle for message and dialog id");
        }

        builder.setMessage(messageString)
                .setPositiveButton(positiveStringId, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mDialogEvent != null)
                            mDialogEvent.onPositiveDialogResualt(dialogId, arguments);
                    }
                })
                .setNegativeButton(negativeStringId, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mDialogEvent != null)
                            mDialogEvent.onNegativeDialogResualt(dialogId, arguments);
                    }
                });

        return builder.create();
    }

    //in do method zir ro age ham override nakonim kar mikone
    @Override
    public void onCancel(DialogInterface dialog) {//in method va method payin ro vase in gozashtim ke masalan vase zamani ke kharej az dialog click kardi khob method onCancel ro farakhani mikone
        //va vase hamin maasalan age khastim badesh ye kar konim ba override kardanesh mitoonim in karo konim va age ham chandta dialog bekhayem mese bala ba inteface ke neveshtim in karo mikonim va dar oonja ba gereftane id kare lazem ro anjam midim
//        super.onCancel(dialog); //chon dar resource in hich kari nemikone oon ro pak mikonim
        Log.d(TAG, "onCancel: starts");
        if (mDialogEvent != null) {
            int dialogId = getArguments().getInt(DIALOG_ID);
            mDialogEvent.onDialogCancelled(dialogId);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {//in method mese onResume() o ina age nanevisi ham kar mikone
        Log.d(TAG, "onDismiss: starts");
        super.onDismiss(dialog);//in method har vaght ke button haye khode dialog ro zadi ya inke kollan dialog napadid she seda zade mishe
        //in super hatman bayad bashe chon age nabashe dar land etefagh haye badi miyofte
    }
}