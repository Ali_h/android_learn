package com.example.ali.tasktimer;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;


public class AddEditActivity extends AppCompatActivity implements AddEditActivityFragment.OnSaveClick, AppDialog.DialogEvent {
    private static final String TAG = "AddEditActivity";
    public static final int DIALOG_ID_CANCEL_EDIT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //add the fragment too the layout
        AddEditActivityFragment fragment = new AddEditActivityFragment();//create fragment

        Bundle arguments = getIntent().getExtras();
        fragment.setArguments(arguments);//set data to fragment

        FragmentManager fragmentManager = getSupportFragmentManager();//for support older version
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home://for up button in toolbar
                Log.d(TAG, "onOptionsItemSelected: on home button in toolbar pressed");
                AddEditActivityFragment fragment = (AddEditActivityFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
                if (fragment.canClose()) {
                    return super.onOptionsItemSelected(item);
                } else {
                    showConfirmationDialog();
                    return true;
                }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showConfirmationDialog() {
        Log.d(TAG, "showConfirmationDialog: starts");

        AppDialog appDialog = new AppDialog();
        Bundle args = new Bundle();

        args.putInt(AppDialog.DIALOG_ID, DIALOG_ID_CANCEL_EDIT);
        args.putString(AppDialog.DIALOG_MESSAGE, getString(R.string.cancelEditDialog_message));
        args.putInt(AppDialog.DIALOG_POSITIVE_RID, R.string.cancelEditDialogPositive_caption);
        args.putInt(AppDialog.DIALOG_NEGATIVE_RID, R.string.cancelEditDialogNegative_caption);

        appDialog.setArguments(args);
        appDialog.show(getSupportFragmentManager(), null);
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: starts");
        super.onResume();
    }

    @Override
    public void onSaveClick() {
        Log.d(TAG, "onSaveClick: starts");
        finish();
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed: starts");

        FragmentManager fragmentManager = getSupportFragmentManager();
        AddEditActivityFragment fragment = (AddEditActivityFragment) fragmentManager.findFragmentById(R.id.fragment);
        if (fragment.canClose()) {//dg inja null check nemikhad chon in fragment ozvi az layout has dg hatman has
            super.onBackPressed();//in baes mishe ke back button kar kone age nabashe kar nemide
        } else {
            showConfirmationDialog();
        }
    }

    @Override
    public void onPositiveDialogResualt(int dialogId, Bundle args) {
        Log.d(TAG, "onPositiveDialogResualt: starts");

        switch (dialogId) {
            case DIALOG_ID_CANCEL_EDIT:
                // no action required
                break;
        }
    }

    @Override
    public void onNegativeDialogResualt(int dialogId, Bundle args) {
        Log.d(TAG, "onNegativeDialogResualt: starts");
        switch (dialogId) {
            case DIALOG_ID_CANCEL_EDIT:
                finish();
                break;
        }
    }

    @Override
    public void onDialogCancelled(int dialogId) {
        Log.d(TAG, "onDialogCancelled: starts");
    }
}
