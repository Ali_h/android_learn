package com.example.ali.tasktimer;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by ali on 9/28/17.
 * <p>
 * content provider for this app . this is only class that knows about {@link AppDatabase} class
 */

public class AppProvider extends ContentProvider {
    private static final String TAG = "AppProvider";

    private AppDatabase mOpenHelper;//baraye dastresi be AppDatabase

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    static final String CONTENT_AUTHORITY = "com.example.ali.tasktimer.provider";
    //To avoid conflicts with other providers, you should use Internet domain ownership (in reverse) as the basis of your provider authority
    // if your Android package name is com.example.<appname>, you should give your provider the authority com.example.<appname>.provider.

    public static final Uri CONTENT_AUTHORITY_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    //public goftim chon mikhayem dar kharej inja besh dastresi dashte bashim age yadet bashe too barname content provide example oonja ke goftim contact contract oon ham ye class bood mese hamin task contract
    //ke uri khodesh ro dasht va ma bejaye inke bedoonim uri oon chi bood az hamin fieldesh ke public tarif shode bood estefade kardim hal ma ham ino public mikonim ta dg nakhayem bedoonim uri chi boode

    private static final int TASKS = 100;//in yek name table ast hala in chera id ham dare ? vase inke age khast ye query fagaht roo ye id bezane
    private static final int TASKS_ID = 101;

    private static final int TIMINGS = 200;
    private static final int TIMINGS_ID = 201;

//    private static final int TASK_TIMINGS = 300;
//    private static final int TASK_TIMINGS_ID = 301;

    private static final int TASK_DURATIONS = 400;
    private static final int TASK_DURATIONS_ID = 401;

    private static UriMatcher buildUriMatcher() {
        Log.d(TAG, "buildUriMatcher: starts");
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);//agar table name daroon uri nabashad no_match ro barmigardoone

        //eg . content://com.example.tasktimer.provider/Tasks
        matcher.addURI(CONTENT_AUTHORITY, TaskContract.TABLE_NAME, TASKS);//parame akhar -> int: the code that is returned when a URI is matched against the given components. Must be positive.

        //eg . content://com.example.tasktimer.provider/Tasks/6
        matcher.addURI(CONTENT_AUTHORITY, TaskContract.TABLE_NAME + "/#", TASKS_ID);

//        matcher.addURI(CONTENT_AUTHORITY , TimingsContract.TABLE_NAME , TIMINGS);
//        matcher.addURI(CONTENT_AUTHORITY , TimingsContract.TABLE_NAME + "/#" , TIMINGS_ID);
//        matcher.addURI(CONTENT_AUTHORITY , DurationsContract.TABLE_NAME , TASK_DURATIONS);
//        matcher.addURI(CONTENT_AUTHORITY , DurationsContract.TABLE_NAME + "/# ", TASK_DURATIONS_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        Log.d(TAG, "onCreate: starts");

        // initialize your content provider on startup. This method is called for all registered content providers on the application main thread at application launch time. It must not perform lengthy operations, or application startup will be delayed.
        mOpenHelper = AppDatabase.getInstance(getContext());//dar hengam init provider AppDatabase ro migirim ta betoonim ro database query bezanim
        return true;//boolean  true if the provider was successfully loaded, false otherwise
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        //query : Use the arguments to select the table to query, the rows and columns to return, and the sort order of the result. Return the data as a Cursor object.

        Log.d(TAG, "query: called with uri : " + uri);
        final int match = sUriMatcher.match(uri);//final chon enmikhayem avaz she
        Log.d(TAG, "query: match is : " + match);

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();//yek method has ke khode android faraham karde
        //ke mitoonim azesh baraye select kardan estefade konim va kafist ke ba mehtode setTable table ro besh begim
        //raveshe digar dar : https://guides.codepath.com/android/Creating-Content-Providers

        switch (match) {
            case TASKS:
                queryBuilder.setTables(TaskContract.TABLE_NAME);//table ie ke mikhaem azesh data begirim ro besh migim
                break;
            case TASKS_ID:
                queryBuilder.setTables(TaskContract.TABLE_NAME);
                long taskId = TaskContract.getTaskId(uri);//uri ro voroodi dadim chon dar in uri voroodi id ma has va bayad bere va uri parse she va id ham bayad long bashe
                queryBuilder.appendWhere(TaskContract.Columns.TASKS_ID + " = " + taskId);//besh where ro ezafe mikonim va id ro besh midim
                break;

            default:
                throw new IllegalArgumentException("unknown uri : " + uri);
        }

        SQLiteDatabase db = mOpenHelper.getReadableDatabase();//getReadableDatabase : chon mikhayem data ro bekhoonim
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);//ba in kar migim ke data ha avaz shode
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        Log.d(TAG, "getType: starts");

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case TASKS:
                return TaskContract.CONTENT_TYPE;

            case TASKS_ID:
                return TaskContract.CONTENT_ITEM_TYPE;

//            case TIMINGS:
//                return TimingsContract.CONTENT_TYPE;
//
//            case TIMINGS_ID:
//                return TimingsContract.CONTENT_ITEM_TYPE;
//
//            case TASK_DURATIONS:
//                return DurationsContract.CONTENT_TYPE;
//
//            case TASK_DURATIONS_ID:
//                return DurationsContract.CONTENT_ITEM_TYPE;

            default:
                throw new IllegalArgumentException("Unknown uri " + uri);//IllegalArgumentException : You may choose to throw this if your provider receives an invalid content URI
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {//ContentValues : This class is used to store a set of values that the ContentResolver can process.
        //ContentValues mese bundle ast hengame estefade azesh
        Log.d(TAG, "insert: strats with uri : " + uri);
        final int match = sUriMatcher.match(uri);
        Log.d(TAG, "insert: match is : " + match);

        final SQLiteDatabase db;//inja getWriteableDatabase nemikonim chon momkene ke aslan match ma ghalat bashe va alaki oomadim va database ro sakhtim ke khoob nis va hamchenin getWriteableDatabase ye khorde kond ast pas ma inja in karo nemikonim

        Uri returnUri;
        long recordId;

        switch (match) {
            case TASKS:
                db = mOpenHelper.getWritableDatabase();
                recordId = db.insert(TaskContract.TABLE_NAME, null, contentValues);//nullColumnHack : null(documention ro bekhoon) Sometimes you want to insert an empty row, in that case ContentValues have no content value, and you should use nullColumnHack.
//                insert : Returns long the row ID of the newly inserted row, or -1 if an error occurred
                if (recordId >= 0) {
                    Log.d(TAG, "insert: recordId is : " + recordId);
                    returnUri = TaskContract.buildTaskUri(recordId);//chera ma inja uri misazim ? vase inke ma yek data jadid varede database karim khob hala age bad khastim in data ro
                    //begirim bayad id oon ro be query bedim dg oon id ro az koja biarim vase hamin inja vaghti ke misazimesh uri ro barash dorost mikonim
                } else {
                    throw new android.database.SQLException("field to insert into " + uri.toString());
                }
                break;

            case TIMINGS:
                db = mOpenHelper.getWritableDatabase();
//                recordId = db.insert(TimingsContract.Timings.buildTimingsUri(recordId));
//                if (recordId >= 0){
//                    returnUri = TimingsContract.Timings.buildTimingsUri(recordId);
//                }else {
//                    throw new android.database.SQLException("failed to insert into " + uri.toString());
//                }
//                break;

            default:
                throw new IllegalArgumentException("unknown uri : " + uri);
        }
        if (recordId >= 0) {
            Log.d(TAG, "insert: set notify change with uri : " + uri);
            getContext().getContentResolver().notifyChange(uri, null);
        } else {
            Log.d(TAG, "insert: nothing insert");
        }

        Log.d(TAG, "exiting insert returning " + returnUri);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        Log.d(TAG, "delete: starts with uri " + uri);
        final int match = sUriMatcher.match(uri);
        Log.d(TAG, "delete: match is :" + match);

        final SQLiteDatabase db;
        int count;
        String selectionCriteria;//for append where cluse for id

        switch (match) {
            case TASKS:
                db = mOpenHelper.getWritableDatabase();
                count = db.delete(TaskContract.TABLE_NAME, selection, selectionArgs);
                break;

            case TASKS_ID:
                db = mOpenHelper.getWritableDatabase();
                long taskId = TaskContract.getTaskId(uri);
                selectionCriteria = TaskContract.Columns.TASKS_ID + " = " + taskId;

                if ((selection != null) && (selection.length() > 0)) {//avval null boodan ro check kon chon age null bashe vali avval length ro check koni oon vaght null exception mide
                    Log.d(TAG, "delete: oooomad inja ");
                    selectionCriteria += " AND (" + selection + ")";
                }
                count = db.delete(TaskContract.TABLE_NAME, selectionCriteria, selectionArgs);
                break;

//            case TIMINGS:
//                db = mOpenHelper.getWritableDatabase();
//                count = db.delete(TimingsContract.TABLE_NAME , selection , selectionArgs);//ba control + q bebin hatamn update ro
//                //update() : return int the number of rows affected
//                //albate ye moshkel has az android studio ke return update bayad long bashe mese insert na int
//                break;
//
//            case TIMINGS_ID:
//                db = mOpenHelper.getWritableDatabase();
//                long timingsId = TimingsContract.getTaskId(uri);//khob inja ma id ro migirim be vasile in tabe getTaskId() ke dar TimingsContract neveshtim
//                selectionCriteria = TimingsContract.Columns._ID + " = " + timingsId;
//
//                if ((selection != null) && (selection.length() > 0)){//khob momkene ke user id ro bede ke vared e in case mishe vali momkene ke yek condition ezafe ham gofte bashe ke in mishe hamoon selection pas ma check mikonim ke age selection null nabood va ye chizi dasht oon ro be selectionCriteria ezafe mikonim
//                    //hamoon tor ke bala ham goftam selection mese hamoon where has vali age karbar khast ye doone row ro update kone ba id miyad inja va selectionCriteria ro vase hamin sakhtim ke id ro begire va age selection ham bood oon ro be selectionCriteria ezafe mikonim
//                    selectionCriteria += " AND (" + selection + ")";
//                }
//                count = db.delete(TimingsContract.TABLE_NAME ,selectionCriteria ,selectionArgs);
//                break;

            default:
                throw new IllegalArgumentException("unknown uri : " + uri);
        }
        if (count > 0) {
            Log.d(TAG, "delete: setting notify change with uri : " + uri);
            getContext().getContentResolver().notifyChange(uri, null);
        } else {
            Log.d(TAG, "delete: nothin delete");
        }

        Log.d(TAG, "delete: ends with coutn : " + count);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String selection, @Nullable String[] selectionArgs) {
        Log.d(TAG, "update: starts with uri " + uri);
        final int match = sUriMatcher.match(uri);
        Log.d(TAG, "update: match is :" + match);

        final SQLiteDatabase db;
        int count;
        String selectionCriteria;//for append where cluse for id

        switch (match) {
            case TASKS:
                db = mOpenHelper.getWritableDatabase();
                count = db.update(TaskContract.TABLE_NAME, contentValues, selection, selectionArgs);
                break;

            case TASKS_ID:
                db = mOpenHelper.getWritableDatabase();
                long taskId = TaskContract.getTaskId(uri);
                selectionCriteria = TaskContract.Columns.TASKS_ID + " = " + taskId;

                if ((selection != null) && (selection.length() > 0)) {//avval null boodan ro check kon chon age null bashe vali avval length ro check koni oon vaght null exception mide
                    Log.d(TAG, "update: ooomad inja");
                    selectionCriteria += " AND (" + selection + ")";
                }
                count = db.update(TaskContract.TABLE_NAME, contentValues, selectionCriteria, selectionArgs);
                break;

//            case TIMINGS:
//                db = mOpenHelper.getWritableDatabase();
//                count = db.update(TimingsContract.TABLE_NAME , values , selection , selectionArgs);//ba control + q bebin hatamn update ro
//                //update() : return int the number of rows affected
//                //albate ye moshkel has az android studio ke return update bayad long bashe mese insert na int
//                break;
//
//            case TIMINGS_ID:
//                db = mOpenHelper.getWritableDatabase();
//                long timingsId = TimingsContract.getTaskId(uri);//khob inja ma id ro migirim be vasile in tabe getTaskId() ke dar TimingsContract neveshtim
//                selectionCriteria = TimingsContract.Columns._ID + " = " + timingsId;
//
//                if ((selection != null) && (selection.length() > 0)){//khob momkene ke user id ro bede ke vared e in case mishe vali momkene ke yek condition ezafe ham gofte bashe ke in mishe hamoon selection pas ma check mikonim ke age selection null nabood va ye chizi dasht oon ro be selectionCriteria ezafe mikonim
//                    //hamoon tor ke bala ham goftam selection mese hamoon where has vali age karbar khast ye doone row ro update kone ba id miyad inja va selectionCriteria ro vase hamin sakhtim ke id ro begire va age selection ham bood oon ro be selectionCriteria ezafe mikonim
//                    selectionCriteria += " AND (" + selection + ")";
//                }
//                count = db.update(TimingsContract.TABLE_NAME , values ,selectionCriteria ,selectionArgs);
//                break;

            default:
                throw new IllegalArgumentException("unknown uri : " + uri);
        }
        if (count > 0) {
            Log.d(TAG, "update: setting notify with uri : " + uri);
            getContext().getContentResolver().notifyChange(uri, null);
        } else {
            Log.d(TAG, "update: nothing update");
        }

        Log.d(TAG, "update: ends with coutn : " + count);
        return count;
    }
}
