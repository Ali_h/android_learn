package com.example.ali.tasktimer;

import java.io.Serializable;

/**
 * Created by ali on 9/30/17.
 */

class TaskHolder implements Serializable {
    public static final long serialVersionUID = 20173009L;

    private long _id;//final nis chon ta ghable insert ma az id khabar nadarim va vaghti insert mikonim id ro barmigardoone vase hamin niyaz darim ke update she
    private final String name;//chon in field ha dar vaghe yek row az database ma hastan pas nabayad ejaze bedim ke taghir konan va nashe barashoon setter gozasht
    private final String description;
    private final int sortOrder;

    public TaskHolder(long _id, String name, String description, int sortOrder) {
        this._id = _id;
        this.name = name;
        this.description = description;
        this.sortOrder = sortOrder;
    }

    public long get_id() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    @Override
    public String toString() {
        return "TaskHolder{" +
                "_id=" + _id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", sortOrder=" + sortOrder +
                '}';
    }
}
