package com.example.ali.tasktimer;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by ali on 10/1/17.
 */

class CursorRecyclerViewAdapter extends RecyclerView.Adapter<CursorRecyclerViewAdapter.TaskViewHolder> {
    private static final String TAG = "CursorRecyclerViewAdapt";
    private Cursor mCursor;//for holding the cursor from another activity

    interface OnTaskClickListener {
        void onEditClick(TaskHolder taskHolder);

        void onDeleteClick(TaskHolder taskHolder);
    }

    private OnTaskClickListener mListener;

    CursorRecyclerViewAdapter(Cursor cursor, OnTaskClickListener listener) {
        Log.d(TAG, "CursorRecyclerViewAdapter: constructor called");
        mCursor = cursor;
        mListener = listener;
    }

    @Override
    public CursorRecyclerViewAdapter.TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: starts and request for new view");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_list_item, parent, false);
        return new TaskViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CursorRecyclerViewAdapter.TaskViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: starts");

        if ((mCursor == null) || (mCursor.getCount() == 0)) {
            Log.d(TAG, "onBindViewHolder: providing instruction");
            holder.name.setText(R.string.instruction_heading);
            holder.description.setText(R.string.instruction_description);
            holder.deleteButton.setVisibility(View.GONE);
            holder.editButton.setVisibility(View.GONE);
            holder.sepraterLine.setVisibility(View.GONE);
        } else {
            if (!mCursor.moveToPosition(position)) {
                throw new IllegalStateException("couldnt move the cursor to position : " + position);
            }
            Log.d(TAG, "onBindViewHolder: cursor position " + mCursor.getPosition());

            final TaskHolder taskHolder = new TaskHolder(mCursor.getLong(mCursor.getColumnIndex(TaskContract.Columns.TASKS_ID)),
                    mCursor.getString(mCursor.getColumnIndex(TaskContract.Columns.TASKS_NAME)),
                    mCursor.getString(mCursor.getColumnIndex(TaskContract.Columns.TASKS_DESCRIPTION)),
                    mCursor.getInt(mCursor.getColumnIndex(TaskContract.Columns.TASKS_SORTORDER)));

            holder.name.setText(taskHolder.getName());
            holder.description.setText(taskHolder.getDescription());
            holder.sepraterLine.setVisibility(View.VISIBLE);
            holder.deleteButton.setVisibility(View.VISIBLE);
            holder.editButton.setVisibility(View.VISIBLE);

            View.OnClickListener buttonListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "onClick: starts");

                    int taskId = view.getId();
                    switch (taskId) {
                        case R.id.tli_deleteButton:
                            mListener.onDeleteClick(taskHolder);
                            break;
                        case R.id.tli_editButton:
                            mListener.onEditClick(taskHolder);
                    }
                }
            };
            holder.deleteButton.setOnClickListener(buttonListener);
            holder.editButton.setOnClickListener(buttonListener);
        }
    }

    /**
     * swap in a new cursor and return the oldCursor
     * the returning oldCursor is <em>not</em> closed
     * in tabe in karo mikone ke newCursor ro migire age data dasht va ba ghabli yeki nabood
     * data ro dar recycler change mikone age ham data nadasht yani recycler bayad khali bashe
     * va invaght ma notifyItemRangeRemoved ro mizanim
     *
     * @param newCursor the new cursor to be used
     * @return returns the previsouly set cursor or null if there wasnt one
     * if the new cursor equal to previsouly cursor return null
     */
    Cursor swapCursor(Cursor newCursor) {
        Log.d(TAG, "swapCursor: starts");
        if (newCursor == mCursor) {
            return null;
        }
        final Cursor oldCursor = mCursor;
        mCursor = newCursor;
        if (newCursor != null) {
            notifyDataSetChanged();
        } else {
            notifyItemRangeRemoved(0, getItemCount());//getItemCount : The total number of items in this adapter.
        }
        return oldCursor;
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: starts");
        if ((mCursor == null) || (mCursor.getCount() == 0)) {
            return 1;
        } else {
            return mCursor.getCount();
        }
    }

    static class TaskViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "TaskHolder";

        TextView name = null;
        TextView description = null;
        TextView sepraterLine = null;
        ImageButton deleteButton = null;
        ImageButton editButton = null;

        public TaskViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "TaskHolder: starts");
            name = itemView.findViewById(R.id.tli_name);
            description = itemView.findViewById(R.id.tli_descriptioin);
            sepraterLine = itemView.findViewById(R.id.sepraterLine);
            deleteButton = itemView.findViewById(R.id.tli_deleteButton);
            editButton = itemView.findViewById(R.id.tli_editButton);
        }
    }
}
