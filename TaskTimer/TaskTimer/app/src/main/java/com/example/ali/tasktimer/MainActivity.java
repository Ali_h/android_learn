package com.example.ali.tasktimer;

import android.annotation.SuppressLint;
import android.support.v7.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.chootdev.csnackbar.Duration;
import com.chootdev.csnackbar.Snackbar;
import com.chootdev.csnackbar.Type;

import static com.example.ali.tasktimer.R.id.frameLayout;

public class MainActivity extends AppCompatActivity implements CursorRecyclerViewAdapter.OnTaskClickListener, AddEditActivityFragment.OnSaveClick,
        AppDialog.DialogEvent {

    private static final String TAG = "MainActivity";
    private boolean mTwoPaneMode = false;//for know in landscape(or tablet) or in phone

    public static final int DIALOG_ID_DELETE = 1;//for delete task
    public static final int DIALOG_ID_CANCEL_EDIT = 2;//for exit app

    private AlertDialog mAlertDialog = null;//for about Dialog

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (findViewById(frameLayout) != null) {
            mTwoPaneMode = true;
            findViewById(frameLayout).setVisibility(View.GONE);
        }

        //COLUMN WE NEED
        String[] projection = {TaskContract.Columns.TASKS_ID,
                TaskContract.Columns.TASKS_NAME,
                TaskContract.Columns.TASKS_DESCRIPTION,
                TaskContract.Columns.TASKS_SORTORDER};

        //CONTENT RESOLVER FOR WORK WITH DATA AND USE QUERY ON THEM
        ContentResolver contentResolver = getContentResolver();

        //CONTENT VALUES FOR VALUES INSERTED FOR INSERT AND UPDATE
        ContentValues values = new ContentValues();//for add data for inserting
        //UPDATE DATA
//        values.put(TaskContract.Columns.TASKS_NAME , "farid");
//        values.put(TaskContract.Columns.TASKS_DESCRIPTION , "mir");
//        values.put(TaskContract.Columns.TASKS_SORTORDER , 2);
//        int count = contentResolver.update(TaskContract.buildTaskUri(2) , values , null , null);//and can use TaskContract.CONTENT_URI for update all the rows
//        Log.d(TAG, "onCreate: " + count + "record(s) updated");

        //UPDATE SEVERAL ROWS WITH WHERE
//        values.put(TaskContract.Columns.TASKS_DESCRIPTION, "DATA CHANGE WITH OR");
//        String selection = TaskContract.Columns.TASKS_NAME + " = 'navid' OR " + TaskContract.Columns.TASKS_NAME + " = " + "'ali'";
//        int count = contentResolver.update(TaskContract.CONTENT_URI, values, selection, null);
//        Log.d(TAG, "onCreate: " + count + "record(s) updated");

        //UPDATE SEVERAL ROWS WITH WHERE AND SELECTIONARGS FOR PEREVENT INJECTION
//        values.put(TaskContract.Columns.TASKS_DESCRIPTION, "DATA CHANGE WITH OR");
//        String selection = TaskContract.Columns.TASKS_NAME + " = ? OR " + TaskContract.Columns.TASKS_NAME + " = ?";
//        String[] selectionArgs = {"ali" , "navid"};
//        int count = contentResolver.update(TaskContract.CONTENT_URI, values, selection, selectionArgs);
//        Log.d(TAG, "onCreate: " + count + "record(s) updated");

        //INSERT DATA
//        values.put(TaskContract.Columns.TASKS_NAME , "farid");//string key hamoon column name ha hastan
//        values.put(TaskContract.Columns.TASKS_DESCRIPTION , "khooie");
//        values.put(TaskContract.Columns.TASKS_SORTORDER , 2);
//        Uri uriInsert = contentResolver.insert(TaskContract.CONTENT_URI , values);//Uri ast chon insert uri barmigardoone

        //DELETE DATA
//        int count = contentResolver.delete(TaskContract.buildTaskUri(2) , null , null);//or CONTENT_URI For uri for delete all row
//        Log.d(TAG, "onCreate: " + count + "record(s) deleted");

        //DELETE DATA WITH WHERE AND SELECTION ARGS FOR INCEJTION
//        String selection = TaskContract.Columns.TASKS_NAME + " = ?";
//        String[] selectionArgs = {"farid"};
//        int count = contentResolver.delete(TaskContract.CONTENT_URI , selection , selectionArgs);
//        Log.d(TAG, "onCreate: " + count + "record(s) deleted");

        //QUERY IN DATA (SELECT)
//      Cursor cursor = contentResolver.query(TaskContract.buildTaskUri(2),// in buildTaskUri(2) miyad id ro 2 mizare va alan faghat data ba id 2 bargardoonde mishe
//        Cursor cursor = contentResolver.query(TaskContract.CONTENT_URI,
//                projection,
//                null,
//                null,
//                TaskContract.Columns.TASKS_ID);
//        if (cursor != null) {
//            Log.d(TAG, "onCreate: number of rows : " + cursor.getCount());//Returns the numbers of rows in the cursor.
//            Log.d(TAG, "onCreate: column count" + cursor.getColumnCount());
//            while (cursor.moveToNext()) {
//                for (int index = 0; index < cursor.getColumnCount(); index++) {
//                    Log.d(TAG, "onCreate: " + cursor.getColumnName(index) + " : " + cursor.getString(index));
//                }
//                Log.d(TAG, "onCreate: ===========================");
//            }
//            cursor.close();
//        }

//        AppDatabase sql = AppDatabase.getInstance(this);
//        final SQLiteDatabase db = sql.getReadableDatabase();//Create and/or open a database. This will be the same object returned by getWritableDatabase()

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.menu_addTask:
                taskAddorEditRequest(null);//passing null for add new task
                break;
            case R.id.menu_generate:
                break;
            case R.id.menu_setting:
                break;
            case R.id.menu_showAbout:
                showAboutDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetTextI18n")//ignore warning in setText
    public void showAboutDialog() {
        Log.d(TAG, "showAboutDialog: starts");
        @SuppressLint("InflateParams") View messageView = getLayoutInflater().inflate(R.layout.about_dialog_custom, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name);
        builder.setIcon(R.drawable.alarm_clock);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mAlertDialog != null && mAlertDialog.isShowing())
                    mAlertDialog.dismiss();
            }
        });
        builder.setView(messageView);

        mAlertDialog = builder.create();
        mAlertDialog.setCanceledOnTouchOutside(true);

//        messageView.setOnClickListener(new View.OnClickListener() {  //in kar khoob nis dar log in proje goftam chera
//            @Override
//            public void onClick(View v) {
//                if(mAlertDialog != null && mAlertDialog.isShowing())
//                    mAlertDialog.dismiss();
//            }
//        });

        TextView version = messageView.findViewById(R.id.about_version);
        version.setText("v" + BuildConfig.VERSION_NAME);

        TextView about_url = messageView.findViewById(R.id.about_url);
        about_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                String url = ((TextView) v).getText().toString();
                intent.setData(Uri.parse(url));
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Snackbar.with(MainActivity.this, null)
                            .message("URL not Found !!")
                            .type(Type.ERROR)
                            .duration(Duration.SHORT)
                            .show();
                }
            }
        });

        mAlertDialog.show();
    }

    private void taskAddorEditRequest(TaskHolder task) {
        Log.d(TAG, "taskAddorEditRequest: starts");
        if (mTwoPaneMode) {
            Log.d(TAG, "taskAddorEditRequest: in landscape or tablet");
            FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
            frameLayout.setVisibility(View.VISIBLE);
            frameLayout.animate().alpha(1F).setDuration(1700).start();

            AddEditActivityFragment fragment = new AddEditActivityFragment();//create fragment

            Bundle args = new Bundle();//create bundle to sent data to fragment
            args.putSerializable(TaskHolder.class.getSimpleName(), task);
            fragment.setArguments(args);//set data to fragment

            FragmentManager fragmentManager = getSupportFragmentManager();//for support older version
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout, fragment);
            fragmentTransaction.commit();

        } else {
            Log.d(TAG, "taskAddorEditRequest: run in a phone");
            Intent intent = new Intent(this, AddEditActivity.class);
            if (task != null) {//eddit a task
                intent.putExtra(TaskHolder.class.getSimpleName(), task);
                startActivity(intent);
            } else {//add a new task
                startActivity(intent);
            }
        }
    }

    //implement recyclerView
    @Override
    public void onEditClick(TaskHolder taskHolder) {
        Log.d(TAG, "onEditClick: starts");
        taskAddorEditRequest(taskHolder);
    }

    //implement recyclerView
    @Override
    public void onDeleteClick(TaskHolder taskHolder) {
        Log.d(TAG, "onDeleteClick: starts");

        AppDialog appDialog = new AppDialog();
        Bundle args = new Bundle();
        args.putInt(AppDialog.DIALOG_ID, DIALOG_ID_DELETE);
        args.putString(AppDialog.DIALOG_MESSAGE, getString(R.string.deldialog_message, taskHolder.getName()));
        args.putInt(AppDialog.DIALOG_POSITIVE_RID, R.string.deldialog_positive_button);

        //for task ID to use for delete in onPositiveDialogResualt
        args.putLong("TaskId", taskHolder.get_id());

        appDialog.setArguments(args);
        appDialog.show(getSupportFragmentManager(), null);
    }

    ////implement AddEditActivityFragment
    @Override
    public void onSaveClick() {
        Log.d(TAG, "onSaveClick: starts");

        FragmentManager fragmentManager = getSupportFragmentManager();//fragmentManager
        Fragment fragment = fragmentManager.findFragmentById(R.id.frameLayout);//find Fragment
        if (fragment != null) {//if exist fragment
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();//start transAction
            fragmentTransaction.remove(fragment);//remove fragment
            fragmentTransaction.commit();//commit remove
            if (findViewById(R.id.frameLayout) != null) {
                findViewById(R.id.frameLayout).setVisibility(View.GONE);
            }
            Log.d(TAG, "onSaveClick: fragment is removed");
        }
    }

    //implement AppDialog
    @Override
    public void onPositiveDialogResualt(int dialogId, Bundle args) {
        Log.d(TAG, "onPositiveDialogResualt: starts");

        switch (dialogId) {
            case DIALOG_ID_DELETE:
                Long taskId = args.getLong("TaskId");//inja age ke taskId dar bundle nabashe ma ghablan null check mikardim vali getLong age chizi toosh nabashe 0 barmigardoone
                //if (BuildConfig.DEBUG && taskId == 0) throw new AssertionError("taskId is zero");
                if (taskId == 0) throw new AssertionError("taskid is zero");
                getContentResolver().delete(TaskContract.buildTaskUri(taskId), null, null);

                //in code payin vase zamani ast ke dar land scape vaghti ke yek task dashtim va mikhstim edit konim khod do ghesmat mishod dg hala age hamoon lahze ke 2ghesmat has deletesh koni oon ghesmate
                //edit mimoone vase hamin in fragment ro delete mikonam
                FragmentManager fragmentManager = getSupportFragmentManager();
                AddEditActivityFragment fragment = (AddEditActivityFragment) fragmentManager.findFragmentById(R.id.frameLayout);
                if (fragment != null) {
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.remove(fragment);
                    transaction.commit();
                }

                Snackbar.with(this, null)
                        .type(Type.SUCCESS)
                        .duration(Duration.SHORT)
                        .message("Task Delete Successful :)")
                        .show();
                break;

            case DIALOG_ID_CANCEL_EDIT:
                // no action required
                break;
        }

    }

    //implement AppDialog
    @Override
    public void onNegativeDialogResualt(int dialogId, Bundle args) {
        Log.d(TAG, "onNegativeDialogResualt: starts");
        switch (dialogId) {
            case DIALOG_ID_DELETE:
                //no action required
                break;
            case DIALOG_ID_CANCEL_EDIT:
                finish();
                break;
        }
    }

    //implement AppDialog
    @Override
    public void onDialogCancelled(int dialogId) {//dar vaghe in method alan vase ma kari nemikone vali age khasti bade dialog chizi ro neshooon bedi khoob ast
        //vali chera dar hamoon method onCancel dar AppDilaog in karo nakardim vase inke age chand ja khastim in dialog ro ejra konim va khastim vase har ja yek kare motafavet bokone
        //nemishe dar hamoon onCancel gof chon faghat in kar baes anjam yek kare sabet mishe vase hamin ba interface pass midim be jahaye dg
        Log.d(TAG, "onDialogCancelled: starts");
    }

    @Override
    public void onBackPressed() {//vaghti ke too navigation back ro mizane in method farakhani mishe
        Log.d(TAG, "onBackPressed: starts");

        //do khate payin vase ine ke fragment ro dar frameLyout peyda kone hala chera az mTwoPane estefade nakardim chon age berim dar land
        //va hanooz button ie nazadim ke fragment ro dar frameLayout add kone yani FrameLayout has vali hanooz daresh fragment ro add nakarde
        //pas nemishe azz in estefade kard chon dar vaghe in tabe in kar mikone ke vaghti ke fragment load shod va chizi dashtim add mikardim
        //va bad ye ho back ro zadim besh yek dialog neshoon bede vali vaghti ke hanooz fragmentie nis ke toosh chizi add kard pas nemishe be
        //mTwoPnae estefade kard
        FragmentManager fragmentManager = getSupportFragmentManager();
        AddEditActivityFragment fragment = (AddEditActivityFragment) fragmentManager.findFragmentById(R.id.frameLayout);
        if ((fragment == null) || fragment.canClose()) {//age jaye in do shart ro avaz konim khata dar barname migirim chon age fragmenti nabashe null mishe vali dar canClose farz ma ine ke fragment has vali data hanooz daresh set nakardim
            super.onBackPressed();//in baes mishe ke back button kar kone age nabashe kar nemide
        } else {
            AppDialog appDialog = new AppDialog();
            Bundle args = new Bundle();

            args.putInt(AppDialog.DIALOG_ID, DIALOG_ID_CANCEL_EDIT);
            args.putString(AppDialog.DIALOG_MESSAGE, getString(R.string.cancelEditDialog_message));
            args.putInt(AppDialog.DIALOG_POSITIVE_RID, R.string.cancelEditDialogPositive_caption);
            args.putInt(AppDialog.DIALOG_NEGATIVE_RID, R.string.cancelEditDialogNegative_caption);

            appDialog.setArguments(args);
            appDialog.show(getSupportFragmentManager(), null);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
//        Log.d(TAG, "onStop: dialog isShowing : " + mAlertDialog.isShowing());
        if (mAlertDialog != null && mAlertDialog.isShowing())
            mAlertDialog.dismiss();
    }
}
