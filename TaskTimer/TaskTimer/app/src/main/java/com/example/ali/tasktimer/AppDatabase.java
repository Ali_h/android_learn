package com.example.ali.tasktimer;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by ali on 9/27/17.
 * <p>
 * Basic database class for application
 * <p>
 * the only class that should use this class is {@link AppProvider}. becuse this class is package private
 */

class AppDatabase extends SQLiteOpenHelper {
    private static final String TAG = "AppDatabase";

    public static final String DATABASE_NAME = "TaskTimer.db";
    public static final int DATABASE_VERSION = 1;

    //implement AppDatabase as a singelton
    private static AppDatabase ourInstance = null;

    //singelton constructor
    private AppDatabase(Context context) {//context is required
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(TAG, "AppDatabase: constructor called");
    }

    /**
     * get an instance of the app's singelton database helper object
     *
     * @param context the content provider context
     * @return a sqlite database helper object
     */
    static AppDatabase getInstance(Context context) {
        if (ourInstance == null) {
            Log.d(TAG, "getInstance: creating a new instance of AppDatabase");
            ourInstance = new AppDatabase(context);
        }
        return ourInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Called automaticly when the database is created for the first time. This is where the creation of tables and the initial population of the tables should happen.
        Log.d(TAG, "onCreate: starts");

//        sSQL = "CREATE TABLE Tasks (_id INTEGER PRIMARY KEY NOT NULL , Name TEXT NOT NULL , Description TEXT , SortOrder INTEGER);"; IN KHOOB NIS CHON HARD CODED HAS VA AGE NAME YEK COLUMN AVAZ SHE MA BE GA MIRIM
        String sSQL = "CREATE TABLE " + TaskContract.TABLE_NAME + " ("
                + TaskContract.Columns.TASKS_ID + " INTEGER PRIMARY KEY NOT NULL, "
                + TaskContract.Columns.TASKS_NAME + " TEXT NOT NULL, "
                + TaskContract.Columns.TASKS_DESCRIPTION + " TEXT, "
                + TaskContract.Columns.TASKS_SORTORDER + " INTEGER);";

        Log.d(TAG, sSQL);
        sqLiteDatabase.execSQL(sSQL);

        Log.d(TAG, "onCreate: ends");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {//Called when the database needs to be upgraded
        Log.d(TAG, "onUpgrade: starts");
        switch (oldVersion) {
            case 1:
                //upgrade logic from version 1
                break;
            default:
                throw new IllegalStateException("onUpgrade() with unknown new version " + newVersion);
        }
        Log.d(TAG, "onUpgrade: ends");

    }
}
