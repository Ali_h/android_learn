package com.example.ali.tasktimer;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.chootdev.csnackbar.Duration;
import com.chootdev.csnackbar.Snackbar;
import com.chootdev.csnackbar.Type;

/**
 * A placeholder fragment containing a simple view.
 */
public class AddEditActivityFragment extends Fragment {
    private static final String TAG = "AddEditActivityFragment";

    public enum FragmentMode {EDIT, ADD}

    private FragmentMode mMode;
    private EditText mName;
    private EditText mDescription;
    private EditText mSortOrder;
    private Button mSave;

    interface OnSaveClick {

        void onSaveClick();
    }

    private OnSaveClick mSaveClick = null;//init in OnAttach()

    public AddEditActivityFragment() {
        Log.d(TAG, "AddEditActivityFragment: constructor called");
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach: starts");
        super.onAttach(context);

        //activity containing this fragment must implement its callback
        Activity activity = getActivity();//Return the Activity this fragment is currently associated with. : yani miyad oon activity ke baes shode in fragment ro seda bezane migire
        if (!(activity instanceof OnSaveClick)) {//inja barresi mikone ke aya oon class implement karde ya na
            throw new ClassCastException(activity.getClass().getSimpleName() + " must implement this callback");
        }
        mSaveClick = (OnSaveClick) getActivity();
    }

    @Override
    public void onDetach() {//vase zamani ke fragment az activity joda mishe
        Log.d(TAG, "onDetach: starts");
        super.onDetach();
        mSaveClick = null;
    }

    public boolean canClose() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: starts");

        View view = inflater.inflate(R.layout.fragment_add_edit, container, false);

        mName = view.findViewById(R.id.addedit_name_task);
        mDescription = view.findViewById(R.id.addedit_task_descrption);
        mSortOrder = view.findViewById(R.id.addedit_task_sortorder);
        mSave = view.findViewById(R.id.addedit_save_task);

//        Bundle arguments = getActivity().getIntent().getExtras();//chon fragment has bayad yek getActivity bazanam avvalesh
        Bundle arguments = getArguments();//for setArgument in addEditActivity

        final TaskHolder task;//chon dar inner class estefade mishe bayad final bashe
        if (arguments != null) {
            Log.d(TAG, "onCreateView: retrive the taskHolder detail");
            task = (TaskHolder) arguments.getSerializable(TaskHolder.class.getSimpleName());
            if (task != null) {
                Log.d(TAG, "onCreateView: edditing the task");
                mMode = FragmentMode.EDIT;
                mName.setText(task.getName());
                mDescription.setText(task.getDescription());
                mSortOrder.setText(Integer.toString(task.getSortOrder()));//chon int bood be string tabdilesh kardam
            } else {
                Log.d(TAG, "onCreateView: adding a new task");
                mMode = FragmentMode.ADD;
            }
        } else {
            Log.d(TAG, "onCreateView: adding a new task with out bundle ");
            task = null;//chon final ast bayad meghdar dahi she
            mMode = FragmentMode.ADD;
        }

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int sortOrder;
                if (mSortOrder.length() > 0) {//age ke sortOrder ro vared karde bood intesh mikonim va vared database esh mikonim age nakarde bood khodemoon 0 mizarim
                    sortOrder = Integer.parseInt(mSortOrder.getText().toString());//inja chon meghdari ke dar editText sortorder has string ast inja be int tabdilesh mikonim
                } else {
                    sortOrder = 0;
                }

                ContentResolver contentResolver = getActivity().getContentResolver();
                ContentValues values = new ContentValues();

                switch (mMode) {
                    case EDIT:
                        Log.d(TAG, "onClick: editting the task");
                        if (!mName.getText().toString().equals(task.getName()))
                            values.put(TaskContract.Columns.TASKS_NAME, mName.getText().toString());
                        if (!mDescription.getText().toString().equals(task.getDescription()))
                            values.put(TaskContract.Columns.TASKS_DESCRIPTION, mDescription.getText().toString());
                        if (sortOrder != task.getSortOrder())
                            values.put(TaskContract.Columns.TASKS_SORTORDER, sortOrder);
                        if (values.size() != 0) {
                            Log.d(TAG, "onClick: updating the task");
                            contentResolver.update(TaskContract.buildTaskUri(task.get_id()), values, null, null);
                            Snackbar.with(getContext(), null)
                                    .type(Type.SUCCESS)
                                    .message("Edit Successful :)")
                                    .duration(Duration.SHORT)
                                    .show();
                        }
                        break;

                    case ADD:
                        if (mName.length() > 0) {
                            Log.d(TAG, "onClick: add the new task");
                            values.put(TaskContract.Columns.TASKS_NAME, mName.getText().toString());
                            values.put(TaskContract.Columns.TASKS_DESCRIPTION, mDescription.getText().toString());
                            values.put(TaskContract.Columns.TASKS_SORTORDER, sortOrder);
                            contentResolver.insert(TaskContract.CONTENT_URI, values);
                            Snackbar.with(getActivity(), null)
                                    .type(Type.SUCCESS)
                                    .duration(Duration.SHORT)
                                    .message("Add Task Successful :)")
                                    .show();
                        } else {
                            mName.requestFocus();
                            mName.setHintTextColor(Color.parseColor("#c7ff2a2a"));
//                            Snackbar.make(view , "Task Name Must Entered !!" , Snackbar.LENGTH_SHORT).show();
                            Snackbar.with(getActivity(), null)//use a library for show snackbar
                                    .type(Type.ERROR)
                                    .message("Task Name Must Entered !!")
                                    .duration(Duration.SHORT)
                                    .show();
                        }
                        break;

                }

                if (mSaveClick != null) {//for check listener not null
                    mSaveClick.onSaveClick();
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume: starts");

        super.onResume();
    }
}
