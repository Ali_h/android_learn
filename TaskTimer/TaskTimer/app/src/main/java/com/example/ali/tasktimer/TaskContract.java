package com.example.ali.tasktimer;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import static com.example.ali.tasktimer.AppProvider.CONTENT_AUTHORITY;
import static com.example.ali.tasktimer.AppProvider.CONTENT_AUTHORITY_URI;

/**
 * Created by ali on 9/27/17.
 */

public class TaskContract {
    //this class is one table of database
    private static final String TAG = "TaskContract";

    static final String TABLE_NAME = "Tasks";

    //Tasks field(column)
    public static class Columns{
        public static final String TASKS_ID = BaseColumns._ID;
        public static final String TASKS_NAME = "Name";
        public static final String TASKS_DESCRIPTION = "Description";
        public static final String TASKS_SORTORDER = "SortOrder";

        //for perevent instantiation
        private Columns(){
            //private constructor help us to prevent instatiation
        }
    }

    /**
     * the URI to access the task table
     */
    public static final Uri CONTENT_URI = Uri.withAppendedPath(CONTENT_AUTHORITY_URI , TABLE_NAME);//uri asli ro ke toosh path nis besh midim ba path ro besh be onvane voroodi midim

    //in do string bala marboot be MIME type ha has ke vase method getType() estefade mishe va bayad be in format bashe hatman
    static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd." + CONTENT_AUTHORITY + "." + TABLE_NAME;//or vnd.android.cursor.dir/ for multiple items.
    static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd." + CONTENT_AUTHORITY + "." + TABLE_NAME;//The returned MIME type should start with vnd.android.cursor.item for a single record

    static Uri buildTaskUri(long taskId){
        Log.d(TAG, "buildTaskUri: starts");
        return ContentUris.withAppendedId(CONTENT_URI , taskId);
    }

    static long getTaskId(Uri uri){
        Log.d(TAG, "getTaskId: starts");
        return ContentUris.parseId(uri);
    }
}
