package com.example.ali.flickrimage;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ali on 9/12/17.
 */

public class RecyclerViewPhoto extends RecyclerView.Adapter<RecyclerViewPhoto.ViewHolder> {
    private static final String TAG = "RecyclerViewPhoto";

    private Context mContext;
    private List<PhotoHolder> photoList;

    public RecyclerViewPhoto(Context context, List<PhotoHolder> photoList) {
        Log.d(TAG, "RecyclerViewPhoto: constructor called");
        mContext = context;
        this.photoList = photoList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: start");

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: starts");
        if ((photoList == null) || (photoList.size() == 0)) {
            holder.photoImage.setImageResource(R.drawable.image);
            holder.photoTitle.setText(R.string.recycler_no_photo);
            holder.photoTitle.setTypeface(null, Typeface.BOLD);
        } else {
            PhotoHolder photo = photoList.get(position);
            holder.photoTitle.setText(photo.getTitle());
            Picasso.with(mContext).load(photo.getImageUrl())
                    .error(R.drawable.image)
                    .placeholder(R.drawable.picture)
                    .into(holder.photoImage);
        }
    }

    @Override
    public int getItemCount() {
        return ((photoList != null) && (photoList.size() != 0) ? photoList.size() : 1);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ViewHolder";
        ImageView photoImage = null;
        TextView photoTitle = null;

        ViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "ViewHolder: constructor called");
            this.photoImage = itemView.findViewById(R.id.photoImage);
            this.photoTitle = itemView.findViewById(R.id.photoTitle);
        }
    }

    void loadNewData(List<PhotoHolder> data) {
        Log.d(TAG, "loadNewData: starts");
        photoList = data;
        notifyDataSetChanged();
    }

    PhotoHolder getPhoto(int position) {//for long click in main activity
        Log.d(TAG, "getPhoto: starts");

        return ((photoList != null) && (photoList.size() != 0) ? photoList.get(position) : null);
    }
}
