package com.example.ali.flickrimage;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements ParseJsonData.OnDataAvailable, RecyclerItemClickListener.OnRecyclerClickListener {
    private static final String TAG = "MainActivity";

    private SearchView mSearchView;
    private RecyclerViewPhoto mRecyclerViewPhoto;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activateToolbar(false);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, this));
        mRecyclerViewPhoto = new RecyclerViewPhoto(this, new ArrayList<PhotoHolder>());
        recyclerView.setAdapter(mRecyclerViewPhoto);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String queryResualt = sharedPreferences.getString(FLICKR_QUERY, "");
        if (queryResualt.length() > 0) {
            downloadData(queryResualt);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu: starts");

        getMenuInflater().inflate(R.menu.menu_main, menu);

        mSearchView = (SearchView) menu.findItem(R.id.serachAction).getActionView();//getActionView hamoon actionViewClass ast ke dar menu_search xml tarif kardim
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);//SearchManager provides access to the system search service
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());//in miyad configure haie ke too searchable.xml ro gozashtim migire
        mSearchView.setSearchableInfo(searchableInfo);
        mSearchView.setIconified(true);

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, "onQueryTextSubmit: called");
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                sharedPreferences.edit().putString(FLICKR_QUERY, query).apply();
                mSearchView.clearFocus();
                mSearchView.setQuery("", false);
                downloadData(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.d(TAG, "onQueryTextChange: called");
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected: starts");

        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDataAvailable(List<PhotoHolder> data, DownloadStatus status) {
        Log.d(TAG, "onDataAvailable: starts");

        if (status == DownloadStatus.OK) {
            mRecyclerViewPhoto.loadNewData(data);
        } else {
            Log.e(TAG, "onDataAvailable: error in available data");
        }

        Log.d(TAG, "onDataAvailable: ends");
    }

    @Override
    public void onItemLongClick(View view, int position) {
        Log.d(TAG, "onItemLongClick: starts");

        Intent intent = new Intent(this, PhotoDetailActivity.class);
        intent.putExtra(PHOTO_TRANSFER, mRecyclerViewPhoto.getPhoto(position));
        startActivity(intent);
    }

    void downloadData(String query) {
        Log.d(TAG, "downloadData: starts");

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        TextView loading = (TextView) findViewById(R.id.loading);

        ParseJsonData parseJsonData = new ParseJsonData(this, "https://api.flickr.com/services/feeds/photos_public.gne", "en-us", true, progressBar , recyclerView , loading);
        parseJsonData.execute(query);

    }
}
