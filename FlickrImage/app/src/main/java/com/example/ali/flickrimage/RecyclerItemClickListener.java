package com.example.ali.flickrimage;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by ali on 9/13/17.
 */

public class RecyclerItemClickListener extends RecyclerView.SimpleOnItemTouchListener {
    private static final String TAG = "RecyclerItemClickListen";

    interface OnRecyclerClickListener {
        void onItemLongClick(View view , int position);
    }

    private OnRecyclerClickListener callbackListener;
    private GestureDetectorCompat mGestureDetector;

    public RecyclerItemClickListener(final Context context, final RecyclerView recyclerView, OnRecyclerClickListener listener) {
        Log.d(TAG, "RecyclerItemClickListener: constructor called");

        callbackListener = listener;
        mGestureDetector = new GestureDetectorCompat(context, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public void onLongPress(MotionEvent e) {
                Log.d(TAG, "onLongPress: starts");

                View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (childView != null && callbackListener != null) {
                    Log.d(TAG, "onLongPress: on long click starts");
                    callbackListener.onItemLongClick(childView , recyclerView.getChildAdapterPosition(childView));
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        Log.d(TAG, "onInterceptTouchEvent: starts");
        if (mGestureDetector != null) {
            boolean resualt = mGestureDetector.onTouchEvent(e);
            Log.d(TAG, "onInterceptTouchEvent: return ---> " + resualt);
            return resualt;
        } else {
            Log.d(TAG, "onInterceptTouchEvent: return false");
            return false;
        }
    }
}
