package com.example.ali.flickrimage;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

/**
 * Created by ali on 9/14/17.
 */

public class BaseActivity extends AppCompatActivity {
    private static final String TAG = "BaseActivity";
    static final String PHOTO_TRANSFER = "photo";
    static final String FLICKR_QUERY = "FLICKR_QUERY";

    void activateToolbar(boolean enableHome){
        Log.d(TAG, "activateToolbar:starts");

        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null){
            Log.d(TAG, "activateToolbar: action bar null");
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            if (toolbar != null){
                setSupportActionBar(toolbar);
                actionBar = getSupportActionBar();
            }
        }

        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(enableHome);
        }
    }
}
