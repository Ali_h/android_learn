package com.example.ali.flickrimage;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by ali on 9/12/17.
 */

enum DownloadStatus {
    INIT, OK, ERROR, PROCESSING
}

class GetJsonData {
    private static final String TAG = "GetJsonData";

    private DownloadStatus mDownloadStatus;

    interface OnDownloadComplete {
        void onDownloadComplete(String data, DownloadStatus status);
    }

    private OnDownloadComplete mCallback;

    GetJsonData(OnDownloadComplete callback) {
        mDownloadStatus = DownloadStatus.INIT;
        mCallback = callback;
    }

    void runInSameThread(String completeURL) {
        Log.d(TAG, "runInSameThread: starts");

        if (mCallback != null) {
            String jsonData = downloadJsonData(completeURL);
            if (jsonData != null) {
                mCallback.onDownloadComplete(jsonData, mDownloadStatus);
            } else {
                Log.e(TAG, "runInSameThread: data have been coming back from downloadJsonData() is null");
            }
        }
    }

    private String downloadJsonData(String data) {
        Log.d(TAG, "downloadJsonData: start");

        HttpsURLConnection connection = null;
        BufferedReader reader = null;

        if (data == null) {
            mDownloadStatus = DownloadStatus.ERROR;
            Log.e(TAG, "downloadJsonData: data have been sent too download is null");
            return null;
        }

        try {
            mDownloadStatus = DownloadStatus.PROCESSING;
            URL url = new URL(data);
            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            int ResponseCode = connection.getResponseCode();
            Log.d(TAG, "downloadJsonData: response code is : " + ResponseCode);

            StringBuilder resualt = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String readLine;
            while (null != (readLine = reader.readLine())) {
                resualt.append(readLine).append("\n");
            }

            mDownloadStatus = DownloadStatus.OK;
            return resualt.toString();

        } catch (MalformedURLException e) {
            Log.e(TAG, "downloadJsonData: url invalid");
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(TAG, "downloadJsonData: IO exception error");
            e.printStackTrace();
        } catch (SecurityException e) {
            Log.e(TAG, "downloadJsonData: security exception need permission ??");
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "downloadJsonData: reader not clos ");
                }
            }
        }

        mDownloadStatus = DownloadStatus.ERROR;
        return null;
    }
}
