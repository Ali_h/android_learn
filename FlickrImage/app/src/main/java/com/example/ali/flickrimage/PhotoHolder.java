package com.example.ali.flickrimage;

import java.io.Serializable;

/**
 * Created by ali on 9/12/17.
 */

class PhotoHolder implements Serializable{
    private static final long serialVersionUID = 1L;

    private String title;
    private String tags;
    private String imageUrl;
    private String imageUrlFullscreen;

    PhotoHolder(String title, String tags, String imageUrl, String imageUrlFullscreen) {
        this.title = title;
        this.tags = tags;
        this.imageUrl = imageUrl;
        this.imageUrlFullscreen = imageUrlFullscreen;
    }

    public String getTitle() {
        return title;
    }

    public String getTags() {
        return tags;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getImageUrlFullscreen() {
        return imageUrlFullscreen;
    }

    @Override
    public String toString() {
        return "PhotoHolder{" +
                "title='" + title + '\'' +
                ", tags='" + tags + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", imageUrlFullscreen='" + imageUrlFullscreen + '\'' +
                '}';
    }
}
