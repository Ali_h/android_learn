package com.example.ali.flickrimage;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class PhotoDetailActivity extends BaseActivity {
    ImageView mImageView;
    TextView title;
    TextView tags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);

        activateToolbar(true);

        Intent intent = getIntent();
        PhotoHolder photo = (PhotoHolder) intent.getSerializableExtra(PHOTO_TRANSFER);
        if (photo != null) {
            mImageView = (ImageView) findViewById(R.id.photoDetailImage);
            title = (TextView) findViewById(R.id.photoDetalTitle);
            tags = (TextView) findViewById(R.id.photoDetalTags);

            title.setText(getString(R.string.title_photo_detail, photo.getTitle()));
            tags.setText(getString(R.string.tags_photo_detail , photo.getTags()));

            Picasso.with(this).load(photo.getImageUrlFullscreen())
                    .error(R.drawable.image)
                    .placeholder(R.drawable.picture)
                    .into(mImageView);
        }
    }
}