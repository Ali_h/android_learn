package com.example.ali.flickrimage;

import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 9/12/17.
 */

class ParseJsonData extends AsyncTask<String, Void, List<PhotoHolder>> implements GetJsonData.OnDownloadComplete {
    private static final String TAG = "ParseJsonData";

    private String baseURL;
    private String languageURL;
    private boolean matchAllURL;
    private List<PhotoHolder> mPhotoList;
    private ProgressBar mProgressBar;
    private TextView loading;
    private RecyclerView mRecyclerView;

    interface OnDataAvailable {
        void onDataAvailable(List<PhotoHolder> data, DownloadStatus status);
    }

    private OnDataAvailable mCallback;

    ParseJsonData(OnDataAvailable callback, String baseURL, String languageURL, boolean matchAllURL , ProgressBar progressBar , RecyclerView recyclerView , TextView textView) {
        this.baseURL = baseURL;
        this.languageURL = languageURL;
        this.matchAllURL = matchAllURL;
        mCallback = callback;
        mProgressBar = progressBar;
        loading = textView;
        mRecyclerView = recyclerView;
        animateLoading(mProgressBar , loading);
    }

    private void animateLoading(ProgressBar progressBar, TextView loading) {
        progressBar.setTranslationY(-500F);
        loading.setTranslationY(-500F);
    }

    @Override
    protected void onPreExecute() {
        Log.d(TAG, "onPreExecute: starts");
        super.onPreExecute();
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressBar.animate().translationYBy(500).setDuration(500);

        loading.setVisibility(View.VISIBLE);
        loading.animate().translationYBy(500).setStartDelay(300).setDuration(500);
        mRecyclerView.setVisibility(View.GONE);
    }

    @Override
    protected void onPostExecute(List<PhotoHolder> data) {
        Log.d(TAG, "onPostExecute: starts");

        mProgressBar.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);

        if (mCallback != null) {
            mCallback.onDataAvailable(data , DownloadStatus.OK);
        }
        super.onPostExecute(data);
    }

    @Override
    protected List<PhotoHolder> doInBackground(String... searchCriteria) {
        Log.d(TAG, "doInBackground: starts");
        String completeURL = createURL(searchCriteria[0], languageURL, matchAllURL);

        GetJsonData getJsonData = new GetJsonData(this);
        getJsonData.runInSameThread(completeURL);

        return mPhotoList;
    }

    private String createURL(String searchCriteria, String languageURL, boolean matchAllURL) {
        Log.d(TAG, "createURL: starts");

        return Uri.parse(baseURL).buildUpon()
                .appendQueryParameter("tags", searchCriteria)
                .appendQueryParameter("tagmode", matchAllURL ? "ALL" : "ANY")
                .appendQueryParameter("lang", languageURL)
                .appendQueryParameter("format", "json")
                .appendQueryParameter("nojsoncallback", "1")
                .build().toString();
    }

    @Override
    public void onDownloadComplete(String data, DownloadStatus status) {
        Log.d(TAG, "onDownloadComplete: starts");
        if (status == DownloadStatus.OK) {
            mPhotoList = new ArrayList<>();
            try {
                JSONObject jsonData = new JSONObject(data);
                JSONArray jsonItem = jsonData.getJSONArray("items");

                for (int index = 0; index < jsonItem.length(); index++) {
                    JSONObject jsonPhotoItem = jsonItem.getJSONObject(index);

                    String jsonTitle = jsonPhotoItem.getString("title");
                    String jsonTag = jsonPhotoItem.getString("tags");
                    JSONObject jsonImageLink = jsonPhotoItem.getJSONObject("media");
                    String jsonImageUrl = jsonImageLink.getString("m");

                    String jsonImageFullscreen = jsonImageUrl.replaceFirst("_m.", "_b.");

                    PhotoHolder photoObject = new PhotoHolder(jsonTitle, jsonTag, jsonImageUrl, jsonImageFullscreen);
                    mPhotoList.add(photoObject);

                    Log.d(TAG, "onDownloadComplete: ends parsing json data : " + photoObject.toString());

                }

            } catch (JSONException e) {
                Log.e(TAG, "onDownloadComplete: error in parsing data");
                e.printStackTrace();
                status = DownloadStatus.ERROR;
            }
        }
        Log.d(TAG, "onDownloadComplete: ends");
    }
}
