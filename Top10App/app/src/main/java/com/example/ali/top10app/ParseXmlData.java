package com.example.ali.top10app;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 9/9/17.
 */

class ParseXmlData extends AsyncTask<String, Void, List<RssHolder>> implements GetXmlData.OnDownloadComplete {
    private static final String TAG = "ParseXmlData";
    private ArrayList<RssHolder> application = null;
    private ProgressBar mProgressBar;
    private TextView mTextView;
    private ListView mListView;

    interface OnDataAvailable {
        void onDtaaAvailable(List<RssHolder> data, DownloadStatus status);
    }

    private final OnDataAvailable callback;

    public ParseXmlData(OnDataAvailable callback, ProgressBar progressBar , TextView textView , ListView listView) {
        this.callback = callback;
        mProgressBar = progressBar;
        mTextView = textView;
        mListView = listView;
    }

    @Override
    protected void onPreExecute() {
        mProgressBar.setVisibility(View.VISIBLE);
        mTextView.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.GONE);
    }

    @Override
    protected void onPostExecute(List<RssHolder> rssHolders) {
        Log.d(TAG, "onPostExecute: starts");
        mProgressBar.setVisibility(View.GONE);
        mTextView.setVisibility(View.GONE);
        mListView.setVisibility(View.VISIBLE);

        if (callback != null) {
            callback.onDtaaAvailable(application, DownloadStatus.OK);
        }

        Log.d(TAG, "onPostExecute: ends");
    }

    @Override
    protected List<RssHolder> doInBackground(String... strings) {
        Log.d(TAG, "doInBackground: starts");

        GetXmlData getXmlData = new GetXmlData(this);
        getXmlData.runInSameTherad(strings[0]);
        Log.d(TAG, "doInBackground: ends");
        return application;
    }

    @Override
    public void onDownloadComplete(String XMLdata, DownloadStatus downloadStatus) {
        Log.d(TAG, "onDownloadComplete: starts");

        if (downloadStatus == DownloadStatus.OK) {
            application = new ArrayList<>();
            boolean inEntry = false;
            boolean get100ImageHeight = false;
            String tagText = "";
            RssHolder currentHolder = null;

            try {
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser parser = factory.newPullParser();
                parser.setInput(new StringReader(XMLdata));
                int eventType = parser.getEventType();//return the type of event like END_DOCUMENT OR START_TAG OR END_TAG
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    String tagName = parser.getName();
                    switch (eventType) {
                        case XmlPullParser.START_TAG:
//                            Log.d(TAG, "onDownloadComplete: starts tag with : " + tagName);
                            if ("entry".equalsIgnoreCase(tagName)) {
                                inEntry = true;
                                currentHolder = new RssHolder();
                            } else if ("category".equalsIgnoreCase(tagName) && inEntry) {
                                String categoryLabel = parser.getAttributeValue(null, "label");
                                if (categoryLabel != null) {
                                    tagText = categoryLabel;
                                }
                            } else if ("image".equalsIgnoreCase(tagName)) {
                                String imagerResoulotion = parser.getAttributeValue(null, "height");
                                if (imagerResoulotion != null) {
                                    get100ImageHeight = "100".equalsIgnoreCase(imagerResoulotion);
                                }
                            }
                            break;

                        case XmlPullParser.TEXT:
                            tagText = parser.getText();
                            break;

                        case XmlPullParser.END_TAG:
//                            Log.d(TAG, "onDownloadComplete: end tag with : " + tagName);
                            if (inEntry) {
                                if ("entry".equalsIgnoreCase(tagName)) {
                                    application.add(currentHolder);
                                    inEntry = false;
                                } else if ("artist".equalsIgnoreCase(tagName))
                                    currentHolder.setArtist(tagText);
                                else if ("name".equalsIgnoreCase(tagName))
                                    currentHolder.setName(tagText);
                                else if ("title".equalsIgnoreCase(tagName))
                                    currentHolder.setTitle(tagText);
                                else if ("summary".equalsIgnoreCase(tagName))
                                    currentHolder.setSummary(tagText);
                                else if ("category".equalsIgnoreCase(tagName))
                                    currentHolder.setCategory(tagText);
                                else if ("image".equalsIgnoreCase(tagName))
                                    if (get100ImageHeight) {
                                        currentHolder.setImageURL(tagText);
                                    }
                            }
                            break;

                        default:
                            Log.d(TAG, "onDownloadComplete: ooops");
                    }

                    eventType = parser.next();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d(TAG, "onDownloadComplete: ends");

        }
    }
}
