package com.example.ali.top10app;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ali on 9/9/17.
 */

public class FeedAdapter extends ArrayAdapter {
    private static final String TAG = "FeedAdapter";

    private final int layoutResource;
    private final LayoutInflater mLayoutInflater;
    private List<RssHolder> application;
    private Context mContext;

    public FeedAdapter(@NonNull Context context, @LayoutRes int resource, List<RssHolder> application) {
        super(context, resource);
        this.application = application;
        mLayoutInflater = LayoutInflater.from(context);
        layoutResource = resource;
        mContext = context;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "getCount: starts");
        return application.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Log.d(TAG, "getView: starts");
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(layoutResource, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        RssHolder currentApp = application.get(position);

        viewHolder.title.setText(mContext.getString(R.string.list_item_title, currentApp.getTitle()));
        viewHolder.name.setText(mContext.getString(R.string.list_item_name, currentApp.getName()));
        viewHolder.artist.setText(mContext.getString(R.string.list_item_artist, currentApp.getArtist()));
        viewHolder.category.setText(mContext.getString(R.string.list_item_category, currentApp.getCategory()));
        if (currentApp.getSummary() != null){
            viewHolder.summary.setText(mContext.getString(R.string.list_item_summary, currentApp.getSummary()));
        }

        return convertView;
    }

    private class ViewHolder {
        private static final String TAG = "ViewHolder";
        private TextView title;
        private TextView name;
        private TextView artist;
        private TextView category;
        private TextView summary;

        ViewHolder(View view) {
            Log.d(TAG, "ViewHolder: starts");
            this.title = (TextView) view.findViewById(R.id.textTitle);
            this.name = (TextView) view.findViewById(R.id.name);
            this.artist = (TextView) view.findViewById(R.id.artist);
            this.category = (TextView) view.findViewById(R.id.category);
            this.summary = (TextView) view.findViewById(R.id.summary);
        }
    }
}
