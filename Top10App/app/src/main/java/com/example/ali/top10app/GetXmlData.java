package com.example.ali.top10app;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by ali on 9/9/17.
 */

enum DownloadStatus {
    PROCCESING, OK, ERROR, INIT
}

class GetXmlData {
    private static final String TAG = "GetXmlData";
    private DownloadStatus mDownloadStatus;

    interface OnDownloadComplete {
        void onDownloadComplete(String data, DownloadStatus status);
    }

    private final OnDownloadComplete callback;

    public GetXmlData(OnDownloadComplete callback) {
        Log.d(TAG, "GetXmlData: constructor called");
        mDownloadStatus = DownloadStatus.INIT;
        this.callback = callback;
    }

    void runInSameTherad(String URL) {
        Log.d(TAG, "runInSameTherad: starts");

        if (callback != null) {
            String data = downloadURL(URL);
            callback.onDownloadComplete(data, mDownloadStatus);
        }

        Log.d(TAG, "runInSameTherad: ends");
    }

    private String downloadURL(String... strings) {
        Log.d(TAG, "downloadURL: starts");
        HttpURLConnection httpURLConnection = null;
        BufferedReader reader = null;

        if (strings == null) {
            mDownloadStatus = DownloadStatus.ERROR;
            Log.e(TAG, "doInBackground: string in doinbackground is empty");
            return null;
        }

        try {
            mDownloadStatus = DownloadStatus.PROCCESING;
            URL url = new URL(strings[0]);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();
            int responseCode = httpURLConnection.getResponseCode();
            Log.d(TAG, "doInBackground: response Code is : " + responseCode);

            StringBuilder resualt = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

            String line;
            while (null != (line = reader.readLine())) {
                resualt.append(line).append("\n");
            }

            mDownloadStatus = DownloadStatus.OK;
            return resualt.toString();

        } catch (ProtocolException | MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SecurityException e) {//for permission
            Log.e(TAG, "doInBackground: security exception need permision ? ");
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        mDownloadStatus = DownloadStatus.ERROR;
        return null;
    }
}
