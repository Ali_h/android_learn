package com.example.ali.top10app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ParseXmlData.OnDataAvailable {
    private static final String TAG = "MainActivity";
    private ListView listView;
    private String feedURL = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topfreeapplications/limit=%d/xml";
    private int feedLimit = 10;
    private boolean clickedMovie = false;
    private String cashedURL = "INVALID";
    private static final String FEED_MOVIE = "feedmovie";
    private static final String FEED_LIMIT = "feedlimit";
    private static final String FEED_URL = "feedurl";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            feedURL = savedInstanceState.getString(FEED_URL);
            feedLimit = savedInstanceState.getInt(FEED_LIMIT);
            clickedMovie = savedInstanceState.getBoolean(FEED_MOVIE);
        }

        downloadURL(String.format(feedURL, feedLimit));
        Log.d(TAG, "onCreate: ends");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu: starts");

        getMenuInflater().inflate(R.menu.feed_menu, menu);
        if (feedLimit == 10) {
            menu.findItem(R.id.top10).setChecked(true);
        } else {
            menu.findItem(R.id.top25).setChecked(true);
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.d(TAG, "onPrepareOptionsMenu: starts");
//        MenuItem limitGroup = menu.findItem(R.id.menuLimit);
//        limitGroup.setEnabled(false);
        if (clickedMovie) {
            menu.setGroupEnabled(R.id.menuLimit, false);
            invalidateOptionsMenu();
        } else {
            menu.setGroupEnabled(R.id.menuLimit, true);
            invalidateOptionsMenu();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected: starts");

        //When you successfully handle a menu item, return true. If you don't handle the menu item, you should call the superclass implementation of onOptionsItemSelected() (the default implementation returns false).
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.menuRefresh:
                cashedURL = "INVALID";
                break;

            case R.id.freeApps:
                feedURL = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topfreeapplications/limit=%d/xml";
                break;

            case R.id.paidApps:
                feedURL = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/toppaidapplications/limit=%d/xml";
                break;

            case R.id.song:
                feedURL = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topsongs/limit=%d/xml";
                break;

            case R.id.album:
                feedURL = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topalbums/limit=%d/xml";
                break;

            case R.id.movie:
                feedURL = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topMovies/xml";
                break;

            case R.id.top10:
            case R.id.top25:
                if (!item.isChecked()) {
                    item.setChecked(true);
                    feedLimit = 35 - feedLimit;
                    Log.d(TAG, "onOptionsItemSelected: " + item.getTitle() + " setting feed limit to " + feedLimit);
                } else {
                    Log.d(TAG, "onOptionsItemSelected: " + item.getTitle() + "  feedlimit unchange");
                }
                break;

            default:
                return super.onOptionsItemSelected(item);
        }

        if (itemId == R.id.movie) {
            clickedMovie = true;
        } else {
            clickedMovie = false;
        }

        downloadURL(String.format(feedURL, feedLimit));

        return true;
    }

    private void downloadURL(String url) {
        Log.d(TAG, "downloadURL: starts");
        if (!url.equalsIgnoreCase(cashedURL)) {
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progresBar);
            TextView loading = (TextView) findViewById(R.id.loading);
            listView = (ListView) findViewById(R.id.listView);
            ParseXmlData parseXmlData = new ParseXmlData(this, progressBar, loading , listView);
            parseXmlData.execute(url);
            cashedURL = url;
            Log.d(TAG, "downloadURL: done");
        } else {
            Log.d(TAG, "downloadURL: url not change");
        }
    }

    @Override
    public void onDtaaAvailable(List<RssHolder> data, DownloadStatus status) {
        Log.d(TAG, "onDtaaAvailable: starts");

        if (status == DownloadStatus.OK) {
            FeedAdapter adapter = new FeedAdapter(this, R.layout.list_view, data);
            listView = (ListView) findViewById(R.id.listView);
            listView.setAdapter(adapter);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(FEED_LIMIT, feedLimit);
        outState.putString(FEED_URL, feedURL);
        outState.putBoolean(FEED_MOVIE, clickedMovie);
        super.onSaveInstanceState(outState);
    }
}
