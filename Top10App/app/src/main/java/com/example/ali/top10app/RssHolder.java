package com.example.ali.top10app;

/**
 * Created by ali on 9/9/17.
 *
 */

class RssHolder {
    private String name;
    private String artist;
    private String title;
    private String category;
    private String summary;
    private String imageURL;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "RssHolder{" +
                "name='" + name + '\'' +
                ", artist='" + artist + '\'' +
                ", title='" + title + '\'' +
                ", category='" + category + '\'' +
                ", summary='" + summary + '\'' +
                ", imageURL='" + imageURL + '\'' +
                '}';
    }
}
