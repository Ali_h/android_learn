package com.example.ali.contentprovider;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private ListView contactName;
    private static final int REQUEST_READ_CODE_CONTACTS = 1;//ba in request_code mitoonim chon too barname momkene permission haye bishtari az ma bekhad mishe hame ro injoori handle kard

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        contactName = (ListView) findViewById(R.id.contactName);//ListView

        final int hasReadContactPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS);//check permission
        //dar api haye balaye 23 karbar in ejaza ro dare ke permission ro kollan deny kone pas ma bayad in jaryan ro handle konim hal khode andriod ye seri framework dare
        //in READ_CONTACTS az manifest oomade bazam migam faghat hamoon user-permission dar manifest ba api zire 23 khoobe vali vase balaye oon bayad cod nevesht
        Log.d(TAG, "onCreate: checkselfpermission = " + hasReadContactPermission);


        if (hasReadContactPermission != PackageManager.PERMISSION_GRANTED){
            Log.d(TAG, "onCreate: request permission");
            ActivityCompat.requestPermissions(this , new String[]{Manifest.permission.READ_CONTACTS} , REQUEST_READ_CODE_CONTACTS);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "fab onClick: starts");
                if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                    //chera nagoftim if(hasReadContactPermission == PackageManager.PERMISSION_GRANTED) ? chon vaghti ke ma kolan dont ask me again ro bezanim dafe bad mirim too setting
                    //va vaghti ke oonja besh permission midim ma age be app bargardim (da hali ke app hanoo run has va restart nashode) va click konim baz permission nadarimm chon
                    //hasReadContactPermission ro update nakardim vali intor ke bala neveshtim dg khodesh hamash check mikone ba systemesh
                    Log.d(TAG, "fab onClick: permission granted");

                    String[] projection = {ContactsContract.Contacts.DISPLAY_NAME_PRIMARY};// list of which columns to return. Passing null will return all columns, which is inefficient.

                    ContentResolver contentResolver = getContentResolver();//ContentResolver execute a query and back the cursor
                    Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI,
                            projection,
                            null,
                            null,
                            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY);
                    //contentResolver.query : oon chizi ke az uri ro mikhayem be dast miyare va mifahme ke kodoom provider bayad send the query requset to it
                    // then gets A cursor back from provider and then return the cursor to our calling code va garanti nis data ie bargardoone pas ma bayad check konim ke in coursor null nabashe
                    //pass client request mide bad contentResolver oon ro migire bad in be provider mige bad ye coursor barmigarde ba in tabe contentResolver.query be provider migim
                    //---> nokte khdoe tabe query() returns Cursor object, which is positioned before the first entry, or null yani dg lazem nis begim if (movetofirst()) chon khodesh dare mige ya avvale ya null

                    if (cursor != null) {
                        List<String> contacts = new ArrayList<String>();
                        while (cursor.moveToNext()) {
                            contacts.add(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY)));
                        }
                        cursor.close();
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this, R.layout.contact_detail, R.id.name, contacts);
                        contactName.setAdapter(adapter);
                    }
                } else {
                    Snackbar.make(view , "Please Grant Access to Show Your Contacts." , Snackbar.LENGTH_LONG).setAction("Grant Access", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d(TAG, "snackbar onClick: starts");
                            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this , Manifest.permission.READ_CONTACTS)){//return false if tik the dont ask agin
                                Log.d(TAG, "snackbar onClick: calling request permission");
                                ActivityCompat.requestPermissions(MainActivity.this , new String[]{Manifest.permission.READ_CONTACTS} , REQUEST_READ_CODE_CONTACTS);
                            }else{
                                Log.d(TAG, "snack bar onClick: lunching setting");
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);//setAction(String) : Set the general action to be performed String: An action name, such as ACTION_VIEW.
                                Uri uri = Uri.fromParts("package" , MainActivity.this.getPackageName() , null);
                                //uri is: consists of schema like https or file or in this case a package
                                //Uri fromParts (String scheme,String ssp,String fragment) pass scheme esh dar inja mishe package va ssp ma dar inja mishe name package
                                Log.d(TAG, "snack bar onClick: uri is = " + uri.toString());
                                intent.setData(uri);
                                MainActivity.this.startActivity(intent);//chon dar yek inner class hastim avvalesh goftim MainActivity.this

                            }
                            Log.d(TAG, "snack bar onClick: ends");
                        }
                    }).show();
                }
                Log.d(TAG, "fab onClick: ends");
            }
        });
    }

    @Override
    //in method mostaghim az tarafe barname ejra nemishe in az tarafe andriod anjam mishe vali ma mitoonim oon ro override konim ta masaln kar haye ezafe tari anjam bedim vali dar in app ma hich kari nemikonim va mitoonim in ro pak konim chon khode android in method ro handle mikone
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //int[] grantResults : in age chandta permission bekhad natije inke user ghabool karde ya na too ine masalan farz kon vase READ_CONTACT va WRITE_CONTACT permission bekhad
        //oon vaght too khoone 0 natije inke ghabool shode ya na READ_CONTACT has va too dovvomi male WRITE_CONTACT has (hala in ke ki avvale va ki dovvom ro nemidoonam)
        //String[] permissions : mige ke che permission e darkhast shode inja masalan permission[0] has READ_CONTACTS
        Log.d(TAG, "onRequestPermissionsResult: starts");
        switch (requestCode) {
            case REQUEST_READ_CODE_CONTACTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                } else {
                    Log.d(TAG, "onRequestPermissionsResult: permission denied");
                }
        }
        Log.d(TAG, "onRequestPermissionsResult: end");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
